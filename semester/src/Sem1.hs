module Sem1 where
import Data.Char

data STerm = Var String
        | Lambda String STerm
        | Apply STerm STerm
    deriving (Eq, Show)

eval1 :: STerm -> Maybe STerm
eval1 term = case term of
    Apply a1@(Apply t1 t2) t3 -> Just $ Apply (getVal a1) t3  
    Apply t1 a1@(Apply t2 t3) -> Just $ Apply t1 (getVal a1)  
    Apply l1@(Lambda (v1) t1) l2 -> Just $ replaceVar v1 l2 t1 (getBoundVariables t1) 
    _ -> Just term
  where getVal val = ret where (Just ret) = eval1 val

eval_ :: STerm -> STerm
eval_ term = case term of
    Apply a1@(Apply (Var v) t2) t3 -> Apply a1 (eval_ t3)  
    Apply a1@(Apply t1 t2) t3 -> eval_ $ Apply (eval_ a1) t3 
    Apply t1 a1@(Apply (Var v) t2) -> Apply (eval_ t1) a1  
    Apply t1 a1@(Apply t2 t3) -> eval_ $ Apply t1 (eval_ a1)  
    Apply l1@(Lambda (v1) t1) l2 -> eval_ $ replaceVar v1 l2 t1 (getBoundVariables t1)  
    _ -> term

replaceVar :: String -> STerm -> STerm -> [String] -> STerm
replaceVar var term inTerm bound = case inTerm of  
    Var v -> if v == var then term else Var v 
    lam@(Lambda (v) (Var v1)) -> if v /= v1 && v1 == var
                    then Lambda (v) new_term 
                    else lam  
    lam@(Lambda (v) (Lambda v2 t)) -> if v == var 
                    then lam
                    else Lambda (v) (replaceVar var new_term (Lambda v2 t) bound) 
    lam@(Lambda (v) (Apply t1 t2)) -> if v == var 
                    then lam
                    else Lambda (v) (replaceVar var new_term (Apply t1 t2) bound) 
    app@(Apply t1 t2) -> Apply (replaceVar var new_term t1 bound) (replaceVar var new_term t2 bound) 
    where new_term = case term of  
                Var v -> if v `elem` bound 
                    then Var (replaceOnNonBoundVar bound "a")
                    else Var v 
                _ -> replaceNonBoundVars (getBoundVariables term ++ bound) bound term 
                                                                                      

replaceNonBoundVars :: [String] -> [String] -> STerm -> STerm
replaceNonBoundVars _ [] term = term
replaceNonBoundVars boundFull bound@(x:xs) term = replaceNonBoundVars boundFull xs (replaceNonBoundVar x term boundFull)

replaceNonBoundVar :: String -> STerm -> [String] -> STerm
replaceNonBoundVar usedVar term bound = case term of
        Var v -> if usedVar == v 
                        then Var (replaceOnNonBoundVar bound "a") 
                        else Var v  
        l@(Lambda (v) t) -> if usedVar == v
                then Lambda (replaceOn) (forceReplaceVar v replaceOn t) 
                else Lambda (v) (replaceNonBoundVar usedVar t bound) 
        Apply t1 t2 -> Apply (replaceNonBoundVar usedVar t1 bound) (replaceNonBoundVar usedVar t2 bound)
    where replaceOn = replaceOnNonBoundVar bound "a"

forceReplaceVar :: String -> String -> STerm -> STerm
forceReplaceVar var replaceOn term = case term of
    Var v -> if v == var then Var replaceOn else Var v
    Lambda (v) t -> Lambda (v) (forceReplaceVar var replaceOn t)
    Apply t1 t2 -> Apply (forceReplaceVar var replaceOn t1) (forceReplaceVar var replaceOn t2)

getBoundVariables :: STerm -> [String]
getBoundVariables term = case term of
    Lambda (v) t -> v : getBoundVariables t
    Apply t1 t2 -> getBoundVariables t1 ++ getBoundVariables t2
    (Var v) -> []

replaceOnNonBoundVar :: [String] -> String -> String
replaceOnNonBoundVar bound var = if var `elem` bound
                            then replaceOnNonBoundVar bound (nextVar var)
                            else var
                 where nextVar [ch] = [chr $ ord ch + 1]