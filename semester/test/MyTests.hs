-- Your tests go here


import Test.Tasty (TestTree(..), defaultMainWithIngredients, testGroup)
import Test.Tasty.Ingredients.Basic (consoleTestReporter, listingTests)
import Test.Tasty.Runners.AntXML (antXMLRunner)

import Test.Tasty (TestTree(..), testGroup, Timeout(..), localOption)
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck

import Control.Arrow
import Data.Function
import Data.List

import Sem1

main :: IO ()
main = defaultMainWithIngredients [antXMLRunner, consoleTestReporter, listingTests] tests

tests :: TestTree
tests = testGroup "Tests"
        [ testGroup "MyTests" myTests
        ]

myTests :: [TestTree]
myTests =  [
            testGroup "Sem1 eval1" [
                       testCase "eval1 (\\x . x) y == y" $ eval1 (Apply (Lambda "x" (Var "x")) (Var "y")) @?= Just (Var "y"),

                       testCase "eval1 (\\x . \\y . x y) (\\z . z) == (\\y . (\\z . z) y)"
                                $ eval1 (Apply (Lambda "x"
                                                       (Lambda "y" (Apply (Var "x") (Var "y"))))
                                               (Lambda "z" (Var "z")))
                                        @?= Just (Lambda "y" (Apply (Lambda "z" (Var "z")) (Var "y"))),

                       testCase "eval1 (\\x . \\y . x y) (\\z . z) (\\u . \\v . v) == (\\y . (\\z . z) y) (\\u . \\v . v)"
                                $ eval1 (Apply (Apply (Lambda "x"
                                                       (Lambda "y" (Apply (Var "x") (Var "y"))))
                                               (Lambda "z" (Var "z")))
                                            (Lambda "u" (Lambda "v" (Var "v"))))
                                        @?= Just (Apply (Lambda "y" (Apply (Lambda "z" (Var "z")) (Var "y")))
                                                    (Lambda "u" (Lambda "v" (Var "v")))),

                       testCase "eval1 (\\x . (\\x . x) x) == (\\x . (\\x . x) x)"
                                $ eval1 (Lambda "x" (Apply (Lambda "x" (Var "x")) (Var "x")))
                                    @?= Just (Lambda "x" (Apply (Lambda "x" (Var "x")) (Var "x"))),

                       testCase "eval1 (\\x . \\y . x) y = (\\y . a)"
                                $ eval1 (Apply (Lambda "x" (Lambda "y" (Var "x"))) (Var "y"))
                                    @?= Just (Lambda "y" (Var "a")),

                       testCase "eval1 (\\y. \\x . y) (\\y . x) = (\\x . (\\y . a))"
                                $ eval1 (Apply (Lambda "y" (Lambda "x" (Var "y"))) (Lambda "y" (Var "x")))
                                    @?= Just (Lambda "x" (Lambda "y" (Var "a"))),

                       testCase "eval1 (\\x. \\y . x) (\\y . y) = (\\y . (\\a . a))"
                                $ eval1 (Apply (Lambda "x" (Lambda "y" (Var "x"))) (Lambda "y" (Var "y")))
                                    @?= Just (Lambda "y" (Lambda "a" (Var "a"))),

                       testCase "eval1 (\\y . (\\y . x y) y) z = (\\y . x y) z"
                                $ eval1 (Apply (Lambda "y"
                                                (Apply (Lambda "y" (Apply (Var "x") (Var "y"))) (Var "y"))) (Var "z"))
                                    @?= Just (Apply (Lambda "y" (Apply (Var "x") (Var "y"))) (Var "z"))
                 ],
            testGroup "Sem1 eval_" [
                       testCase "eval_ (\\x . x) y == y" $ eval_ (Apply (Lambda "x" (Var "x")) (Var "y")) @?= Var "y",

                       testCase "eval_ (\\x . \\y . x y) (\\z . z) == (\\y . (\\z . z) y)"
                                $ eval_ (Apply (Lambda "x"
                                                       (Lambda "y" (Apply (Var "x") (Var "y"))))
                                               (Lambda "z" (Var "z")))
                                        @?= Lambda "y" (Apply (Lambda "z" (Var "z")) (Var "y")),

                       testCase "eval_ (\\x . \\y . x y) (\\z . z) (\\u . \\v . v) == (\\u . \\v . v)"
                                $ eval_ (Apply (Apply (Lambda "x"
                                                       (Lambda "y" (Apply (Var "x") (Var "y"))))
                                               (Lambda "z" (Var "z")))
                                            (Lambda "u" (Lambda "v" (Var "v"))))
                                        @?= Lambda "u" (Lambda "v" (Var "v")),

                       testCase "eval_ (\\x . (\\x . x) x) == (\\x . (\\x . x) x)"
                                $ eval_ (Lambda "x" (Apply (Lambda "x" (Var "x")) (Var "x")))
                                    @?= Lambda "x" (Apply (Lambda "x" (Var "x")) (Var "x")),

                       testCase "eval_ (\\x . \\y . x) y = (\\y . a)"
                                $ eval_ (Apply (Lambda "x" (Lambda "y" (Var "x"))) (Var "y"))
                                    @?= Lambda "y" (Var "a"),

                       testCase "eval_ (\\x . \\a . x a) a = (\\a . b a)"
                                $ eval_ (Apply (Lambda "x"
                                            (Lambda "a" (Apply (Var "x") (Var "a")))) (Var "a"))
                                    @?= Lambda "a" (Apply (Var "b") (Var "a")),

                       testCase "eval_ (\\y. \\x . y) (\\y . x) = (\\x . (\\y . a))"
                                $ eval_ (Apply (Lambda "y" (Lambda "x" (Var "y"))) (Lambda "y" (Var "x")))
                                    @?= Lambda "x" (Lambda "y" (Var "a")),

                       testCase "eval_ (\\x. \\y . x) (\\y . y) = (\\y . (\\a . a))"
                                $ eval_ (Apply (Lambda "x" (Lambda "y" (Var "x"))) (Lambda "y" (Var "y")))
                                    @?= Lambda "y" (Lambda "a" (Var "a")),

                       testCase "eval_ (\\a . \\x . (\\b . b y) a x) (\\a . \\b . b z) = (\\x . (\\b . b y) (\\a . \\c . c z) x)"
                                $ eval_ (Apply
                                  	    (Lambda "a"
                                  	        (Lambda "x"
                                  	          (Apply (Apply (Lambda "b" (Apply (Var "b") (Var "y"))) (Var "a")) (Var "x"))))
                                  	(Lambda "a" (Lambda "b"
                                  				    (Apply (Var "b") (Var "z")))))
                                     @?= Lambda "x"
                                                    (Apply
                                                        (Apply (Lambda "b" (Apply (Var "b") (Var "y")))
                                                               (Lambda "a" (Lambda "c" (Apply (Var "c") (Var "z")))))
                                                        (Var "x")),

                       testCase "eval_ (\\a . \\x . b a x) (\\a . \\b . b z) = (\\x . b (\\a . \\b . b z) x)"
                                  $ eval_ (Apply
                                            (Lambda "a" (Lambda "x" (Apply (Var "b") (Apply (Var "a") (Var "x")))))
                                            (Lambda "a" (Lambda "b" (Apply (Var "b") (Var "z")))))
                                    @?= Lambda "x" (Apply (Var "b") (Apply
                                                            (Lambda "a" (Lambda "b" (Apply (Var "b") (Var "z"))))
                                                            (Var "x"))),

                       testCase "eval_ (\\y . \\a . y a) (\\a . a y) = (\\a . (\\b . b y) a)"
                                $ eval_ (Apply
                                            (Lambda "y" (Lambda "a" (Apply (Var "y") (Var "a"))))
                                            (Lambda "a" (Apply (Var "a") (Var "y"))))
                                    @?= Lambda "a" (Apply (Lambda "b" (Apply (Var "b") (Var "y"))) (Var "a")),

                       testCase "eval_ (\\x . \\y . x x y) (\\a . a y) b = c c b"
                                $ eval_ (Apply
                                            (Apply (Lambda "x" (Lambda "y" (Apply (Apply (Var "x") (Var "x")) (Var "y"))))
                                                    (Lambda "a" (Apply (Var "a") (Var "y"))))
                                            (Var "b"))
                                    @?= Apply (Apply (Var "b") (Var "b")) (Var "b")
                 ]
            ]

