 module HW4
( Show' (..)
, A(..)
, C(..)
, Set(..)
, symmetricDifference
, fromBool
, fromInt
) where
 
class Show' a where
show' :: a -> String
 
 data A = A Int
| B 
 data C = C Int Int

instance Show' A where
    show' (A int) = "A " ++ numToStr int
    show' B = "B"
 
 data C = C Int Int
instance Show' C where
    show' (C a b) = "C " ++ numToStr a ++ " " ++ numToStr b

numToStr int = if int < 0 then "(" ++ show int ++ ")" else show int
symmetricDifference :: Set a -> Set a -> Set a
symmetricDifference (Set f1) (Set f2) = Set $ \elem -> let elem_in_f1 = f1 elem
                                                           elem_in_f2 = f2 elem
                      in (elem_in_f1 || elem_in_f2) && not (elem_in_f1 && elem_in_f2)
 
tru = \t -> (\f -> t)
fls = \t f -> f

 -- fromBool - переводит булевское значение в кодировку Чёрча
fromBool bool
    | bool = tru
    | otherwise = fls
 
 -- fromInt - переводит число в кодировку Чёрча
fromInt n
    | n == 0 = \s z -> z
    | otherwise = \s z -> s (fromInt (n - 1) s z)