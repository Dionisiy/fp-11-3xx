module HW3
       ( foldr'
       , groupBy'
       , Either' (..)
       , either'
       , lefts'
       , rights'
       ) where

-- foldr' - правая свёртка
-- foldr' + b0 [an,...,a1,a0] =
--    = (an + ... + (a1 + (a0 + b0)))
foldr' :: (a -> b -> b) -> b -> [a] -> b
foldr' func startVal list
    | null list = startVal  --Если список элементов пустой
    | otherwise = func firstElem (foldr' func startVal remainingElements)
    where firstElem : remainingElements = list

-- Группировка
-- > groupBy' (==) [1,2,2,2,1,1,3]
-- [[1],[2,2,2],[1,1],[3]]
groupBy' :: (a -> a -> Bool) -> [a] -> [[a]]
groupBy' cmp [] = []
groupBy' cmp (head:arr) = (head : resArr) : groupBy' cmp otherElems
    where (resArr, otherElems) = gatherByCmp cmp head arr
          gatherByCmp cmp head [] = ([], [])  --gatherByCmp собирает элементы списка в два списка - в первом
                                              --будут первые элементы, для которых cmp-функция дала True, остальные во втором списке
          gatherByCmp cmp head remaining@(head2:arr) = if cmp head head2 then (head2 : nextArr, remainingArr) else ([], remaining)
              where (nextArr, remainingArr) = gatherByCmp cmp head arr

-- Сумма типов a и b
data Either' a b = Left' a
                 | Right' b
                 deriving (Show)
-- Например, Either String b -
-- может быть результат типа b
-- или ошибка типа String

-- Из функций (a -> c) и (b -> c) получить функцию
-- над суммой a и b
-- Например,
-- > either' length id (Left' [1,2,3])
-- 3
-- > either' length id (Right' 4)
-- 4
-- > :t either' length id
-- Either [Int] Int -> Int
either' :: (a -> c) -> (b -> c) -> Either' a b -> c
either' f g eab = case eab of
    (Left' a) -> f a  --Если eab = Left', то применяем первую функцию
    (Right' b) -> g b --В противном случае применяем к значению вторую функцию

-- Получение значений соответствующих слагаемых
lefts' :: [Either' a b] -> [a]
lefts' [] = []
lefts' arr = map leftVal [a | a <- arr, isLeft' a]
            where isLeft' (Left' a) = True
                  isLeft' (Right' b) = False
                  leftVal (Left' a) = a

rights' :: [Either' a b] -> [b]
rights' [] = []
rights' arr = map rightVal [a | a <- arr, isRight' a]
            where isRight' (Left' _) = False
                  isRight' (Right' _) = True
                  rightVal (Right' b) = b

