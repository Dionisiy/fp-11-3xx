module HW2
       ( Contact (..)
       , isKnown
       , Term (..)
       , eval
       , simplify
       ) where

data Contact = On
             | Off
             | Unknown

isKnown :: Contact -> Bool
isKnown = onOff
    where onOff On = True
          onOff Off = True
          onOff _ = False

data Term = Mult Term Term      -- умножение
          | Add Term Term       -- сложение
          | Sub Term Term       -- вычитание
          | Const Int           -- константа
     deriving Eq

eval :: Term -> Int
eval term = case term of
    (Mult term1 term2) -> applyFunc (*) term1 term2
    (Add term1 term2) -> applyFunc (+) term1 term2
    (Sub term1 term2) -> applyFunc (-) term1 term2
    (Const int) -> int
    where applyFunc func t1 t2 = func (eval t1) (eval t2)

-- Раскрыть скобки
-- Mult (Add (Const 1) (Const 2)) (Const 3) ->
-- Add (Mult (Const 1) (Const 3)) (Mult (Const 2) (Const 3))
-- (1+2)*3 -> 1*3+2*3
simplify :: Term -> Term
simplify term = simplifyRememberPrev term (Const 0)

simplifyRememberPrev :: Term -> Term -> Term
simplifyRememberPrev term prevTerm
    | term == prevTerm = term
    | otherwise = justSimplifyIt term

justSimplifyIt :: Term -> Term
justSimplifyIt (Mult left (Add b1 b2))  = Add (simplify (Mult left b1)) (simplify (Mult left b2))
justSimplifyIt (Mult (Add b1 b2) right) = Add (simplify (Mult right b1)) (simplify (Mult right b2))
justSimplifyIt (Mult left (Sub b1 b2)) = simplify (Sub (simplify (Mult left b1)) (simplify (Mult left b2)))
justSimplifyIt (Mult (Sub b1 b2) right) = simplify (Sub (simplify (Mult right b1)) (simplify (Mult right b2)))
justSimplifyIt (Sub left (Add b1 b2)) = simplify (Sub (simplify (Sub left b1)) (simplify b2))
justSimplifyIt (Sub left (Sub b1 b2)) = Add (simplify (Sub left b1)) (simplify b2)
justSimplifyIt (Sub left (Const i)) = Sub (simplify left) (Const i)
justSimplifyIt (Add left right) = Add (simplify left) (simplify right)
justSimplifyIt term@(Mult left (Mult b1 b2)) = simplifyRememberPrev (Mult (simplify left) (simplify (Mult b1 b2))) term
justSimplifyIt term@(Mult (Mult b1 b2) right) = simplifyRememberPrev (Mult (simplify right) (simplify (Mult b1 b2))) term
justSimplifyIt term@(Sub left (Mult b1 b2)) = simplifyRememberPrev (Sub (simplify left) (simplify (Mult b1 b2))) term
justSimplifyIt other = other
