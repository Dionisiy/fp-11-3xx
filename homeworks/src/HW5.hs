-- 2016-04-05 / 2016-04-12

module HW5
       ( Parser (..)
       , dyckLanguage
       , Arith (..)
       , arith
       , Optional (..)
       ) where

{--==========  PARSER ==========-}

type Err = String
data Parser a = Parser
                { parse :: String ->
                  Either Err (a, String) }

instance Functor Parser where
  fmap f p = Parser $ \s -> case parse p s of
    Left err -> Left err
    Right (x, rest) -> Right (f x, rest)

instance Applicative Parser where
  pure a = Parser $ \s -> Right (a,s)
  (<*>) pf pa = Parser $ \s -> case parse pf s of
    Left err -> Left err
    Right (f,r1) -> case parse pa r1 of
      Left err -> Left err
      Right (x,r2) -> Right (f x, r2)

anyChar :: Parser Char
anyChar = Parser $ \s -> case s of
  [] -> Left "No chars left"
  (c:cs) -> Right (c, cs)

many :: Parser a -> Parser [a]
many p = Parser $ \s -> case parse p s of
  Left _ -> Right ([],s)
  Right (x,r1) -> case parse (many p) r1 of
    Left _ -> Right ([x],r1)
    Right (xs,r2) -> Right (x:xs, r2)

matching :: (Char -> Bool) -> Parser Char
matching p = Parser $ \s -> case s of
  [] -> Left "No chars left"
  (c:cs) | p c -> Right (c, cs)
         | otherwise -> Left $ "Char " ++ [c] ++
                        " is wrong"

digit :: Parser Char
digit = matching (\c -> '0'<=c && c<='9')

char :: Char -> Parser Char
char c = matching (==c)

number :: Parser Int
number = pure read <*> many digit
           -- read <$> many digit

(<|>) :: Parser a -> Parser a -> Parser a
(<|>) pa pb = Parser $ \s -> case parse pa s of
  Left err -> parse pb s
  Right a -> Right a

{--========  END PARSER ========-}

-- ((())())
dyckLanguage :: Parser String
dyckLanguage = Parser $ \s -> if correctBraces s 0 then Right (s, "") else Left "Parse failed."
    where correctBraces "" count = count == 0
          correctBraces (brace:left) count = if brace == '('
                            then correctBraces left (count + 1)
                            else correctBraces left (count - 1)
 
 data Arith = Plus Arith Arith
            | Minus Arith Arith
            | Mul Arith Arith
            | Number Int
            | SyntheticOper Char
            deriving (Eq,Show)
 
isOperation :: Char -> Bool
isOperation symbol = symbol `elem` "+-*/"

priority :: Char -> Int
priority operation = case operation of
    '(' -> 0
    ')' -> 1
    '+' -> 2
    '-' -> 2
    '*' -> 3
    '/' -> 3

data Token = Oper Char | Num Int

parseToRPN :: String -> [Char] -> [Token] -> [Token]
parseToRPN "" [] output = output
parseToRPN "" (s:otherStack) output = parseToRPN "" otherStack (output++[Oper s])
parseToRPN str@(symbol:left) stack output
    | null stack && isOperation symbol || symbol == '(' = parseToRPN left (symbol : stack) output
    | isOperation symbol = extractOpersFromStack left stack symbol output
    | symbol == ')' = extractTillOpeningBrace left stack output
    | otherwise = checkEither (parse number str)
        where checkEither (Right (num, str2)) = parseToRPN str2 stack (output ++ [Num num])

extractTillOpeningBrace left stack output =
        case stack of
            [] -> parseToRPN left stack output
            oper : otherOpers
                | oper == '(' -> parseToRPN left otherOpers output
                | otherwise -> extractTillOpeningBrace left otherOpers (output ++ [Oper oper])
extractOpersFromStack leftStr stack symbol output =
        case stack of
            [] -> parseToRPN leftStr (symbol:stack) output
            oper : otherOpers
                | (priority oper) > (priority symbol) -> extractOpersFromStack leftStr otherOpers symbol (output ++ [Oper oper])
                | otherwise -> parseToRPN leftStr (symbol:oper:otherOpers) output

parseRPN :: [Arith] -> Int -> Either Err Arith
parseRPN [Number n1] _ = Right (Number n1)
parseRPN (a1:a2:(SyntheticOper o):[]) _
    | o == '+' = Right (Plus a1 a2)
    | o == '-' = Right (Minus a1 a2)
    | o == '*' = Right (Mul a1 a2)
parseRPN tokens i = case tokens!!i of
    SyntheticOper o
        | i < 2 || length tokens < 3 -> Left ""
        | o == '+' -> parseRPN ((getBefore tokens (i - 2))++[Plus a1 a2]++(getAfter tokens i)) 0
        | o == '-' -> parseRPN ((getBefore tokens (i - 2))++[Minus a1 a2]++(getAfter tokens i)) 0
        | o == '*' -> parseRPN ((getBefore tokens (i - 2))++[Mul a1 a2]++(getAfter tokens i)) 0
                where (a1, a2) = (tokens!!(i - 2), tokens!!(i - 1))
                      getBefore tokens idx = take idx tokens
                      getAfter tokens idx = drop (idx + 1) tokens
    _ -> parseRPN tokens (i + 1)
parseRPN _ _ = Left ""

toArith :: [Token] -> [Arith]
toArith [] = []
toArith ((Oper o):other) = (SyntheticOper o) : (toArith other)
toArith ((Num n):other) = (Number n) : (toArith other)

 arith :: Parser Arith
arith = Parser $ \s -> case parseRPN (toArith (parseToRPN s [] [])) 0 of
    Left _ -> Left "Can't parse the expression."
    right@(Right arith) -> Right (arith, "")
 
                 deriving (Eq,Show)
 
 instance Functor Optional where
  fmap f opt = case opt of
    NoParam -> NoParam
    Param a -> Param (f a)
 
 instance Applicative Optional where
  pure a = Param a
  opt1 <*> opt2 = case opt1 of
    NoParam -> NoParam
    Param f -> case opt2 of
        NoParam -> NoParam
        Param a -> Param (f a)

